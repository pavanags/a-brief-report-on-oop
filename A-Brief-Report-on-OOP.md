# A Brief Report on OOP 

## Introduction

Object-Oriented Programming Concept is a popular way of coding practice because It is easier to understand . 
SO what is OOP?
The answer is really simple. In this approach, we consider everything as an object and our whole programming manipulation is revolving around the objects instead of the logic or the functions.

Here we are discussing the concepts independent of any particular language.
Some examples are taken from different programming languages.

Let’s take a look at the principles of OOP.


## Objects:
As we discussed earlier in OOP practices we need to consider everything as an object. The basic characteristics of an object are that every object has something(characteristics) and does something. The has part is normally called ‘Property’ and the does part consists of methods. In terms of technicality, Object is an instance of a class.

Example:

```JavaScript
//Object Literal method:

let carObject = {
  //Property || "HAS" Part:
  name: "Tesla Roadster",
  fuel: "Electric battery-powered",
  release: 2008,

  //Methods || "DOES" part:
  getCar: function () {
    return `${carObject.name} was ${carObject.name
      .split(" ")
      .shift()}'s First car and it was fully ${
      carObject.fuel
    }. The car was brought into the market in the year ${carObject.release}.`;
  },
};
console.log(carObject.getCar());
```

```
Javascript
//Object constructor:
function companyObject(company_name, company_location) {
  this.company_name = company_name;
  this.company_location = company_location;
}

//creating new instance of objects.
let companyDetails = new companyObject("MountBlue Technologies", "Bangalore");

console.log(
  `${companyDetails.company_name} is located in ${companyDetails.company_location}`
);
```
Note: You can also create objects using [Object.create()] (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Working_with_Objects) in JavaScript. 

## Classes
[JavaScript classes, introduced in ECMAScript 2015, are primarily syntactical sugar over JavaScript's existing prototype-based inheritance. The class syntax does not introduce a new object-oriented inheritance model to JavaScript.](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes)

Normally we call class as the blueprint of any project. But in javascript, class is a 'special function'. It has two parts to it. Class declaration and class expressions.

 But why are we discussing class here? 
 It has its significance in OOP. 
 Lets see a snippet of code to understand it better.
 
 ```
 JavaScript
 //ES6 syntax
 class person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }

  getPerson() {
    return `Hi, My name is ${this.name} and I am ${this.age} years old`;
  }
}

let person1 = new person("John doe", 25);
let person2 = new person("Uzumaki naruto", 28);

console.log(person1.getPerson());
console.log(person2.name);
 ```
 
 So how does the class help us achieve OOP principles?
 
## Inheritence
Inheritance in simple words means that the properties and methods one class inherits from the other. (This can be different in terms of javascript because Javascript uses prototype based OOP. [know more](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain#:~:text=When%20it%20comes%20to%20inheritance,another%20object%20called%20its%20prototype.)) It is very useful because it encourages code re-usability. If we have similar properties in different classes we can use extends instead of rewriting and entire class.

Lets take a Java code to demonstrate inheritance.

```
Java

class Animal{
	
	public String name;
	public double height;
	public double avgWeight;
	
	public Animal(String name, double height, double avgWeight) {
		this.name = name;
		this.height = height;
		this.avgWeight = avgWeight;
	}
	
	public String toString() {
		return "This is "+name+" and "+name+"'s average weight is "+avgWeight+" It grows up to "+height+" ft high." ;
	}
}

class Doggo extends Animal{
	public int lifeSpan;

	public Doggo(String name, double height, double avgWeight, int lifeSpan) {
		super(name, height, (float) avgWeight);
		this.lifeSpan = lifeSpan;
	}
	
	public String toString() {
		return super.toString() + " Its average life span is "+lifeSpan;
	}
}


public class inheritence {
	public static void main(String[] args) {
		
		Doggo d = new Doggo("Tommy", 2.0, 35.5, 12);
		System.out.println(d.toString());
	}

	}

```

As you can see here the Doggo class extends to its parent class which is animal class, and by doing so it gets access to all the animal class properties as well as its methods. Inheritance is a great tool to enforce code re-usability.


## Encapsulation
Encapsulation is a way to secure the data from being manipulated and setting a boundary or limitation to the data so that the data is not accessible outside the defined boundary. This means the data is not defined with public access modifier in java, but instead with a private access modifier. Even if we inherit this class, we will not get access to the private data present inside the class.

In other words, data hiding is achieved to ensure better security to our data and thus it prevents data corruption.

But in the case of JavaScript, we do not have any access modifiers available. But there is a way to encapsulate the data by changing the way we define the data. If Block-level access is given to the data, You will not have any access to it from outside the block.

```
Java

import java.util.*;
class Human{
	private int brainIQ;
	private int heartBeat;
	public void setbrainAndHeart(int x,int y) {
		brainIQ=x;
		heartBeat=y;
	}
	public int getBrainAndHeart () {
		return brainIQ;
	}
	
	
}

public class Encapsulation {
	public static void main(String[] args) {
		Human h=new Human() ;
		h.setbrainAndHeart(110,73);
		System.out.println(h.getBrainAndHeart());
		
	}
}
```


## Abstraction

Abstraction is a process where only data that is useful/essential is shown and certain data is hidden.
Here is an example of how abstraction is achieved.


```
Java

import java.util.Scanner;

abstract class Shape{
	abstract void input();
	abstract void compute();
	float area;
	void disp() {
		System.out.println(area);
	}
}

class Square extends Shape{
	float length;
	void input() {
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the length of The Square");
		length=scan.nextFloat();
	}
	void compute() {
		area=length*length;
	}
}

class rectangle extends Shape{
	float length;
	float width;
	void input() {
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the length of The Rectangle");
		length=scan.nextFloat();
		System.out.println("Enter the width of The Rectangle");
		width=scan.nextFloat();
	}
	void compute() {
		area=length*width;
	}
}

class circle extends Shape{
	float dia;
	void input() {
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the dia of The Circle");
		dia=scan.nextFloat();
	
	}
	void compute() {
		area=(float) ((Math.PI*dia*dia)/4);
	}
}

class Result {
	void calculate(Shape ref) {
		ref.input();
		ref.compute();
		ref.disp();
	}
}


public class Abstraction {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Square sq=new Square();
		rectangle r=new rectangle();
		circle c=new circle();
		Result re=new Result();
		re.calculate(sq);
		re.calculate(r);
		re.calculate(c);
		
	}

}

```

## Polymorphism

Polymorphism is the ability of one object to take many forms. In OOP use of Polymorphism can be seen when a Parent class reference is used to refer to a child class object. 

Some advantages of polymorphism are:

1. Code Re-usability.
2. One variable can store multiple data types.
	[know more](https://www.onlinetutorialspoint.com/java/oops/types-of-polymorphism-and-advantages.html)

```
Java
import java.util.*;

abstract class Match{
	private int currentScore;
	private float currentOver;
	private int target;
	float Runrate;
	int ballsRemaining;
	abstract void input();
	abstract float calculateRunrate();
	abstract int calculateBalls();
	abstract void display(); 
	
	public float getCurrentOver() {
		return currentOver;
	}
	public void setCurrentOver(float currentOver) {
		this.currentOver = currentOver;
	}
	public int getCurrentScore() {
		return currentScore;
	}
	public void setCurrentScore(int currentScore) {
		this.currentScore = currentScore;
	}
	public int getTarget() {
		return target;
	}
	public void setTarget(int target) {
		this.target = target;
	}
	int calculateRun() {
		int RunsReq=(getTarget()-getCurrentScore());
		return RunsReq;
	}
}

class ODI extends Match{
	
	void input() {
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the current Score");
		setCurrentScore(scan.nextInt());
		System.out.println("Enter the current Over");
		setCurrentOver(scan.nextFloat());
		System.out.println("Enter the target");
		setTarget(scan.nextInt());
	}
	float calculateRunrate() {
		Runrate=(getTarget()-getCurrentScore())/(50-getCurrentOver());
		return Runrate;
	}
	int calculateBalls() {
		int over=(int) getCurrentOver();
		float curBall=(getCurrentOver()-over)*10;
		ballsRemaining=(int) ((50*6)-(over*6)-curBall);
		return ballsRemaining;
	}
	
	void display() {
		{
			System.out.println("Required Runrate is"+" "+Runrate);
			System.out.println(ballsRemaining+" "+"Balls remaining");
		}
	}
}

class TEST extends Match{
	void input() {
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the current Score");
		setCurrentScore(scan.nextInt());
		System.out.println("Enter the current Over");
		setCurrentOver(scan.nextFloat());
		System.out.println("Enter the target");
		setTarget(scan.nextInt());
	}
	
	float calculateRunrate() {
		Runrate=(getTarget()-getCurrentScore())/(90-getCurrentOver());
		return Runrate;
	}
	int calculateBalls() {
		int over=(int) getCurrentOver();
		float curBall=(getCurrentOver()-over)*10;
		ballsRemaining=(int) ((90*6)-(over*6)-curBall);
		return ballsRemaining;
	}
	
	void display() {
		{
			System.out.println("Required Runrate is"+" "+Runrate);
			System.out.println(ballsRemaining+" "+"Balls remaining");
		}
}
}

class T20 extends Match{
	void input() {
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the current Score");
		setCurrentScore(scan.nextInt());
		System.out.println("Enter the current Over");
		setCurrentOver(scan.nextFloat());
		System.out.println("Enter the target");
		setTarget(scan.nextInt());
}
	float calculateRunrate() {
		Runrate=(getTarget()-getCurrentScore())/(20-getCurrentOver());
		return Runrate;
	}
	int calculateBalls() {
		int over=(int) getCurrentOver();
		float curBall=(getCurrentOver()-over)*10;
		ballsRemaining=(int) ((20*6)-(over*6)-curBall);
		return ballsRemaining;
	}
	void display() {
		{
			System.out.println("Required Runrate is"+" "+Runrate);
			System.out.println(ballsRemaining+" "+"Balls remaining");
		}
}
}


//The below Code here shows how to achieve polymorphism.
//*************************************
class Calculate{
	void result(Match ref) {
		ref.input();
		ref.calculateRunrate();
		ref.calculateBalls();
		ref.display();
		
		}
}
//*************************************

public class Cricket {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ODI odi=new ODI();
		TEST t=new TEST();
		T20 tt=new T20();
		Calculate ca=new Calculate();
		System.out.println("ENTER THE FORMAT");
		System.out.println("1.ODI");
		System.out.println("2,TEST");
		System.out.println("3.T20");
		Scanner scan=new Scanner(System.in);
		int f=scan.nextInt();
			if(f==1) {
				ca.result(odi);
			}
			else if(f==2) {
				ca.result(t);
			}
			else if(f==3) {
				ca.result(tt);
			}
			else {
				System.out.println("Please select Either 1,2 or 3");
			}
	}

}
```

## Conclusion

OOP technique has many advantages that allow us to maintain our codebase effectively. Let’s take a look at the advantages of using OOP.

1. Effective in problem-solving.
2. Better code re-usability through inheritance.
3. Less data redundancy.
4. Beter code maintenance.
5. Security.
6. Polymorphism flexibility.
7. Easier to troubleshoot.


